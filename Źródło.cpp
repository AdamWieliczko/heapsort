#include <iostream>
using namespace std;

class Kopiec
{
public:
	int arraysize = 10;
	int array[10] = { 2, 4, 5, 1, 3, 7, 6, 8, 10, 9 };
	int variable; //ona sluzy tylko do tego zeby non stop nie tworzyc sobie variable przy heapsorcie oraz kazdym heapify

	int potomek_lewy(int i)
	{
		return ((i << 1));
	}

	int potomek_prawy(int i)
	{
		return ((i << 1) + 1);
	}

	int przodek(int i)
	{
		return (i >> 1);
	}

	void heapify(int array[], int i, int arraysize)
	{
		int najwiekszy;
		int lewy_i = potomek_lewy(i);
		int prawy_i = potomek_prawy(i);

		if (lewy_i < arraysize && array[lewy_i] > array[i])
		{
			najwiekszy = lewy_i;
		}
		else
		{
			najwiekszy = i;
		}

		if (prawy_i < arraysize && array[prawy_i] > array[najwiekszy])
		{
			najwiekszy = prawy_i;
		}
		if (najwiekszy != i)
		{
			variable = array[i];
			array[i] = array[najwiekszy];
			array[najwiekszy] = variable;
			heapify(array, najwiekszy, arraysize);
		}
	}

	void buildheap(int array[])
	{
		for (int i = (arraysize / 2) - 1; i >= 0; i--)
		{
			heapify(array, i, arraysize);
		}
	}

	void heapsort(int array[])
	{
		buildheap(array);

		for (int i = arraysize - 1; i > 0; i--)
		{
			variable = array[i];
			array[i] = array[0];
			array[0] = variable;
			heapify(array, 0, i);
		}
	}

	void printarray(int array[])
	{
		for (int i = 0; i < arraysize; ++i)
		{
			cout << array[i] << " ";
		}
		cout << endl;
	}
};


int main()
{
	Kopiec something;
	something.heapsort(something.array);
	something.printarray(something.array);
	return 0;
}